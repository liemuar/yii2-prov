<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'frontend/web/css/bootstrap.min.css',
        'frontend/web/css/styles.css',
        'frontend/web/css/font-awesome.min.css',
        'frontend/web/css/component.css',
        'frontend/web/css/owl.carousel.css',
        'frontend/web/css/owl.theme.css',
        'frontend/web/css/style.css',
        'frontend/web/css/green.css',
      
    ];
    public $js = [
 
       'frontend/web/js/jquery.min.js',
       'frontend/web/js/jquery.easing.min.js',
       'frontend/web/js/bootstrap.min.js',
       'frontend/web/js/classie.js',
       'frontend/web/js/modalEffects.js',
       'frontend/web/js/waypoints.min.js',
       'frontend/web/js/jquery.counterup.min.js',
       'frontend/web/js/SmoothScroll.js',
       'frontend/web/js/jquery.stellar.min.js',
       'frontend/web/js/jquery.nav.js',
       'frontend/web/js/owl.carousel.min.js',
       'frontend/web/js/app.js',
       'frontend/web/js/switcher.js',
   
   
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
		// 'yii\bootstrap\BootstrapPluginAsset',
  
    ];
}

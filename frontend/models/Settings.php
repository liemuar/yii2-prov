<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string|null $titleMain
 * @property string|null $servicesTitle
 * @property string|null $ContactTitle
 * @property string|null $BgPhotoHome
 * @property string|null $BgPhotoServices
 * @property int|null $created_at
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer'],
            [['titleMain', 'servicesTitle', 'ContactTitle'], 'string', 'max' => 50],
            [['BgPhotoHome', 'BgPhotoServices'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titleMain' => 'Title Main',
            'servicesTitle' => 'Services Title',
            'ContactTitle' => 'Contact Title',
            'BgPhotoHome' => 'Bg Photo Home',
            'BgPhotoServices' => 'Bg Photo Services',
            'created_at' => 'Created At',
        ];
    }
}

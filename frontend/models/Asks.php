<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "asks".
 *
 * @property int $id
 * @property string|null $Name
 * @property string|null $Mail
 * @property string|null $Message
 * @property int|null $created_at
 */
class Asks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Message'], 'string'],
            [['created_at'], 'integer'],
            [['Name', 'Mail'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
            'Mail' => 'Mail',
            'Message' => 'Message',
            'created_at' => 'Created At',
        ];
    }
}

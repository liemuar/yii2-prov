<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "footer".
 *
 * @property int $id
 * @property string|null $leftTitle
 * @property string|null $centerTitle
 * @property string|null $centerText1
 * @property string|null $centerText2
 * @property string|null $centerLink2
 * @property string|null $rightTitle
 * @property string|null $rightText1
 * @property int|null $created_at
 */
class Footer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer'],
            [['leftTitle', 'centerTitle', 'rightTitle'], 'string', 'max' => 50],
            [['centerText1', 'centerText2', 'centerLink2', 'rightText1'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leftTitle' => 'Left Title',
            'centerTitle' => 'Center Title',
            'centerText1' => 'Center Text1',
            'centerText2' => 'Center Text2',
            'centerLink2' => 'Center Link2',
            'rightTitle' => 'Right Title',
            'rightText1' => 'Right Text1',
            'created_at' => 'Created At',
        ];
    }
}

<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Settings;

/**
 * SettingsSearch represents the model behind the search form of `frontend\models\Settings`.
 */
class SettingsSearch extends Settings
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at'], 'integer'],
            [['titleMain', 'servicesTitle', 'ContactTitle', 'BgPhotoHome', 'BgPhotoServices'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Settings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'titleMain', $this->titleMain])
            ->andFilterWhere(['like', 'servicesTitle', $this->servicesTitle])
            ->andFilterWhere(['like', 'ContactTitle', $this->ContactTitle])
            ->andFilterWhere(['like', 'BgPhotoHome', $this->BgPhotoHome])
            ->andFilterWhere(['like', 'BgPhotoServices', $this->BgPhotoServices]);

        return $dataProvider;
    }
}

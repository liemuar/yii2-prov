<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Asks */
/* @var $form ActiveForm */
?>
<div class="Ask pt-1">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Message') ?>
    <?= $form->field($model, 'created_at') ?>
    <?= $form->field($model, 'Name') ?>
    <?= $form->field($model, 'Mail') ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
   

</div><!-- Ask -->
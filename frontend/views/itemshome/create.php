<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Itemshome */

$this->title = 'Create Itemshome';
$this->params['breadcrumbs'][] = ['label' => 'Itemshomes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="itemshome-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

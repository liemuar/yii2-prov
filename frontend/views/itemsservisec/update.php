<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Itemsservisec */

$this->title = 'Update Itemsservisec: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Itemsservisecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="itemsservisec-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

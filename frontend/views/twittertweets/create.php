<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Twittertweets */

$this->title = 'Create Twittertweets';
$this->params['breadcrumbs'][] = ['label' => 'Twittertweets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="twittertweets-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

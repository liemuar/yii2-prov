<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Asks */
/* @var $form ActiveForm */
?>
<div class="Ask p-5">

    <section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title text-center">Contact Us</h3>
        <div class="titleHR"><span></span></div>

          
 



<div class="form-group">

</div>


<?php $form = ActiveForm::begin(); ?>
          <div class="col-xs-12">
            <div class="row">
              <div class="form-group col-xs-6">
               
                <?= $form->field($model, 'Message') ?>
              <div class="error" id="err-name" style="display: none;">Please enter name</div>
              </div>
              <div class="form-group col-xs-6">
          
                <?= $form->field($model, 'Name') ?>
                <div class="error" id="err-emailvld" style="display: none;">E-mail is not a valid format</div>
              </div>
            </div>
            <div class="row">
            


              <div class="form-group col-xs-12">
            
                <?= $form->field($model, 'Mail') ?>
                <div class="error" id="err-message" style="display: none;">Please enter message</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <p class="text-center con_sub_text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam
                  aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo
                  labore Stet clita ea et gubergren.</p>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 text-center">
                <div id="ajaxsuccess">E-mail was successfully sent.</div>
                <div class="error" id="err-form" style="display: none;">There was a problem validating the form please
                  check!</div>
                <div class="error" id="err-timedout">The connection to the server timed out!</div>
                <div class="error" id="err-state"></div>
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
              </div>
            </div>
          </div>
          <?php ActiveForm::end(); ?>
      </div> <!-- end col-md-12 -->
    </div> <!-- end row -->
  </div> <!-- container -->
</section>
</div><!-- Ask -->

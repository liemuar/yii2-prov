<?php

/* @var $this yii\web\View */
use frontend\models\Asks;
use yii\helpers\Html;
// подключаем виджет для работы с формами. use yii\bootstrap\ActiveForm (стилизованная версия под bootstrap 3)
use yii\widgets\ActiveForm; 


$this->title = 'Delex HTML5 Free Responsive Template | Template Stock';
?>

<!-- /HOME -->
<section class="main-home" id="home">
  <div class="home-page-photo" style="	background-image: url(<?php foreach ($settings as $setting){
        echo $setting->BgPhotoHome;} ?>);"></div> <!-- Background image -->
  <div class="home__header-content">
    <div id="main-home-carousel" class="owl-carousel">
    <?php foreach ($itemsHome as $itemHome):?>
      <div>
    
        <h1 class="intro-title"><?= $itemHome->title?></h1>
        <p class="intro-text"><?= $itemHome->description?></p>
        <a class="btn btn-custom" href="#">Get started</a>
      </div>
      <?php endforeach;?>
      <!--slide item like paragraph-->
    </div>
  </div>
</section>
<!-- /End HOME -->

<!-- / SERVICES -->
<section id="services">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="title text-center"><?php foreach ($settings as $setting){
        echo $setting->servicesTitle;} ?></h3>
        <div class="titleHR"><span></span></div>
      </div>
    </div>

    <div class="row">
    <?php foreach ($Itemsservisec as $Itemservisec):?>
      <div class="col-sm-4">
        <!-- Service-item -->
        <div class="text-center services-item">
          <div class="col-wrapper">
            <div class="icon-border">
              <i class="icon-design-graphic-tablet-streamline-tablet color-l-orange"></i>
            </div>
            <h5><?=$Itemservisec->title;?></h5>
            <p><?=$Itemservisec->description  ;?></p>
          </div>
        </div>
      </div>
      <?php endforeach; ?>

    </div>
    <!--/.row -->

    
  </div>
  <!--/.container -->
</section>
<!-- / End services-->


<!-- TWITTER TWEET -->
<section class="twitter_tweet parallax" style="	background: url(frontend/web/images/pattern1.png), url(<?php foreach ($settings as $setting){
        echo $setting->BgPhotoServices;} ?>);" id="twitter_tweet" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-center">
        <div id="testi-carousel" class="owl-carousel owl-spaced">
        <?php foreach ($Twittertweets as $Twittertweet):?>
            <div>
            <i class="fa fa-twitter"></i>
            <h5>
            <?= $Twittertweet->description?>
            </h5>
            <p>  <?= $Twittertweet->name?></p>
          </div>
          <!--testimonials item like paragraph-->
 
   
      <?php endforeach; ?>
      
        </div>
      </div> <!-- end col-md-8 -->
    </div> <!-- end row -->
  </div> <!-- container -->
</section>
<!-- End TWITTER TWEET -->

<!-- CONTACT -->

<!-- End CONTACT -->
<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Footer;

/**
 * FooterSearch represents the model behind the search form of `backend\models\Footer`.
 */
class FooterSearch extends Footer
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at'], 'integer'],
            [['leftTitle', 'centerTitle', 'centerText1', 'centerText2', 'centerLink2', 'rightTitle', 'rightText1'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Footer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'leftTitle', $this->leftTitle])
            ->andFilterWhere(['like', 'centerTitle', $this->centerTitle])
            ->andFilterWhere(['like', 'centerText1', $this->centerText1])
            ->andFilterWhere(['like', 'centerText2', $this->centerText2])
            ->andFilterWhere(['like', 'centerLink2', $this->centerLink2])
            ->andFilterWhere(['like', 'rightTitle', $this->rightTitle])
            ->andFilterWhere(['like', 'rightText1', $this->rightText1]);

        return $dataProvider;
    }
}

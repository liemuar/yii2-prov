<?php
use yii\helpers\Html;

use andrewdanilov\adminpanel\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $siteName string */
/* @var $directoryAsset false|string */

?>

<div class="page-left">
	<div class="sidebar-heading"><?= $siteName ?></div>
	<?= Menu::widget(['items' => [
		['label' => 'Dashboard', 'url' => ['/site/index'], 'icon' => 'tachometer-alt'],
		[],
		['label' => 'Блог'],
		['label' => 'Настройки', 'url' => ['/settings'], 'icon' => ['gg' => 'newspaper', 'type' => 'regular']],
		['label' => 'Сервисы', 'url' => ['/itemsservisec'], 'icon' => ['gg' => 'newspaper', 'type' => 'regular']],
		['label' => 'Меню', 'url' => ['/menu'], 'icon' => ['symbol' => 'gg', 'type' => 'regular']],
		['label' => 'Home', 'url' => ['/itemshome'], 'icon' => ['symbol' => 'gg', 'type' => 'regular']],
		['label' => 'Твиты', 'url' => ['/twittertweets'], 'icon' => ['symbol' => 'gg', 'type' => 'regular']],
		['label' => 'Футтер', 'url' => ['/footer'], 'icon' => ['symbol' => 'gg', 'type' => 'regular']],
		
		[],
		['label' => 'Система'],
		['label' => 'Пользователи', 'url' => ['/user/admin/index'], 'icon' => 'users'],
	]]) ?>
 <?= Html::a('ВЫХОД',
                        ['/user/security/logout'],
                        ['data-method' => 'post', 'class' => 'btn', 'style'=>'border-radius: 30px 3px;']
                    ) ?>

</div>

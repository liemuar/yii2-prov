<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Itemshome */

$this->title = 'Update Itemshome: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Itemshomes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="itemshome-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Itemsservisec */

$this->title = 'Create Itemsservisec';
$this->params['breadcrumbs'][] = ['label' => 'Itemsservisecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="itemsservisec-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

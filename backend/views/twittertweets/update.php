<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Twittertweets */

$this->title = 'Update Twittertweets: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Twittertweets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="twittertweets-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

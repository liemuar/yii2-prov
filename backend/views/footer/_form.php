<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Footer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="footer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'leftTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'centerTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'centerText1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'centerText2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'centerLink2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rightTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rightText1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FooterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="footer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'leftTitle') ?>

    <?= $form->field($model, 'centerTitle') ?>

    <?= $form->field($model, 'centerText1') ?>

    <?= $form->field($model, 'centerText2') ?>

    <?php // echo $form->field($model, 'centerLink2') ?>

    <?php // echo $form->field($model, 'rightTitle') ?>

    <?php // echo $form->field($model, 'rightText1') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

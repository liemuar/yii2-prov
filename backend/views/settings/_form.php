<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'titleMain')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'servicesTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ContactTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'BgPhotoHome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'BgPhotoServices')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

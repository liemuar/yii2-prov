<?
namespace backend\controllers;
 use dektrium\user\controllers\AdminController as BaseAdminController;
use Yii;

class AdminController extends BaseAdminController
{

  public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%itemsservisec}}`.
 */
class m210625_120740_create_itemsservisec_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%itemsservisec}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50),
            'description' => $this->text(),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%itemsservisec}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%twittertweets}}`.
 */
class m210625_120705_create_twittertweets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%twittertweets}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'description' => $this->text(),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%twittertweets}}');
    }
}

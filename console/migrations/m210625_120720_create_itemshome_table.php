<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%itemshome}}`.
 */
class m210625_120720_create_itemshome_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%itemshome}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50),
            'description' => $this->text(),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%itemshome}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m210628_094528_user_record_create
 */
class m210628_094528_user_record_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'administrator',
            'email' => 'administrator@localhost.lc',
            'password_hash' => '$2y$13$b94CY5bXZyA.VnJrD8KWtuxNQqJiUZ/X7D7rSAHuvCDju7L6VsnM.',
            'auth_key' => 'Ijw2fHuPrgqh9gbTtmYMpuXUzmiH51Tn',
            'created_at' => 1499239593,
            'confirmed_at' => 1499239593,
            'updated_at' => 1499239593,
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'superadmin',
            'type' => '1',
            'description' => 'Superadmin',
            'rule_name' => NULL,
            'data' => 1499239593,
            'created_at' => 1499239593,
            'updated_at' => 1499239593,
        ]);
        $this->insert('{{%auth_assignment}}', [
            'item_name' => 'superadmin',
            'user_id' => '1',
            'created_at' => 1499239593,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210628_094528_user_record_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210628_094528_user_record_create cannot be reverted.\n";

        return false;
    }
    */
}

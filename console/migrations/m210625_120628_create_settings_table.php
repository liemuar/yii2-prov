<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m210625_120628_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'titleMain' => $this->string(50),
            'servicesTitle' => $this->string(50),
            'ContactTitle' => $this->string(50),
            'BgPhotoHome' => $this->string(255),
            'BgPhotoServices' => $this->string(255),
            'created_at' => $this->integer(),
            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contactform}}`.
 */
class m210625_120757_create_contactform_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%contactform}}', [
            'id' => $this->primaryKey(),
            'titleName' => $this->string(50),
            'titleMail' => $this->string(50),
            'titleMessage' => $this->string(50),
            'description' => $this->text(),
            'created_at' => $this->integer(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%contactform}}');
    }
}

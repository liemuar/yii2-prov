<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%asks}}`.
 */
class m210628_114005_create_asks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%asks}}', [
            'id' => $this->primaryKey(),
            'Name' => $this->string(50),
            'Mail' => $this->string(50),
            'Message' => $this->text(),
            'created_at' => $this->integer(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%asks}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%footer}}`.
 */
class m210625_120637_create_footer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%footer}}', [
            'id' => $this->primaryKey(),
            'leftTitle' => $this->string(50),
            'centerTitle' => $this->string(50),
            'centerText1' => $this->string(100),
            'centerText2' => $this->string(100),
            'centerLink2' => $this->string(100),
            'rightTitle' => $this->string(50),
            'rightText1' => $this->string(100),
            'created_at' => $this->integer(),

         
           



        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%footer}}');
    }
}
